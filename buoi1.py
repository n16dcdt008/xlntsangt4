# câu 1a
s = 'Hi John, welcome to python programming for beginner!'
# a. kiem tra xem trong chuoi s co tu 'python' ko?
a = 'python'
b = a in s
# câu 1b
a=s.count('python')
if a>0:
    s="python"
else :
    s=0
print(s)
# câu 1c
x=s.count('o')
print("số lần xuất hiện chữ O :",x)
# câu 1d
arr=s.split(' ')
print(
    'kích thước mảng  :',len(arr))
# câu 2a
l = [23, 4.3, 4.2, 31, 'python', 1, 5.3 , 9, 1.7]
l.remove('python')
print("chuỗi mới: ",l)
# câu 2b
l.sort()
print("chuỗi sắp xếp theo thứ tự tăng dần: ",l)
l.sort(reverse=True)
print("chuỗi được sắp xếp theo thứ tự giảm dần:",l)
#câu 2c
a=l.count(4.2)
if a>0:
    print("4.2 có xuất hiện trong chuỗi !")
else :
    print("số 4.2 không xuất hiện trong chuỗi !")
#câu3a
A = {1, 2, 3, 4, 5, 7}
B = {2, 4, 5, 9, 12, 24}
C = {2, 4, 8}
print("A sau khi add C",A|C)
print("B sau khi add C",B|C)
#câu3b
print("giao điểm của A và B :",A&B)
#câu3c
C2=A|B
print("A hợp B",C2)
# câu3d
C3=A-B
print("phần tử có trong A ko có trong B",C3)
# câu3e
print("chiều dài A và B",len(C2))
# câu3f
print("số lớn nhất trong A và B",max(C2))
# câu 3G
print("số nhỏ nhất trong các số ở trong A mà ko có trong B",min(C3))
# câu 4a
T=(1, 'python', [2,3],(4,5))
# câu4b
A=(1, 'python', [2,3],(4,5))
U=A[0]
U=A[1]
U=A[2]
U=A[3]
# câu 4c
print(T[3])
#câu4d
T2=([2,3],)
T3=T+T2
print("T sau khi thêm [2,3] :",T3)
#câu4e
# câu4F
T=list(T3)
print("chuyển thành list:",T)
# câu5a
dic1 = {1:10, 2:20}
dic2 = {3:30, 4:40}
dic3 = {5:50, 6:60}
dic1.update(dic2)
dic1.update(dic3)
print(dic1)
#cau 5b
c = {'a': 1, 'b': 4, 'c': 2}
d= sorted(c.items(), key=lambda x: x[1])
print(d)
# câu6a
# câu 7
nl = ['Python', 'Bill', 'Trump', 'Java','Pascal']
# câu 8
def Count(str):
    upper, lower, number, special = 0, 0, 0, 0
    for i in range(len(str)):
        if str[i] >= 'A' and str[i] <= 'Z':
            upper += 1
        elif str[i] >= 'a' and str[i] <= 'z':
            lower += 1
        elif str[i] >= '0' and str[i] <= '9':
            number += 1
        else:
            special += 1
    print('Upper case letters:', upper)
    print('Lower case letters:', lower)
    print('Number:', number)
    print('Special characters:', special)
# câu 8
import numpy
import random
import re
import string
lst = (numpy.random.choice(50, 1000, replace=True))
print(lst)
x = int(input('nhap vao so x = '))
def count(lst, x):
    dem = 0
    for so in lst:
        if (so == x):
            dem = dem + 1
    return dem
print('số {} xuất hiện {} lần'.format(x, count(lst, x)))
# câu9
import matplotlib.pyplot as plt
n=50
x = numpy.arange(n)
y = 0.2*numpy.cos(x)
plt.plot(x,y,'r-',linewidth=2)
plt.show()
# câu10
import numpy as np
A = np.array([[1 , 2 , 3], [4 , 5 , 6]])
B = np.array([[2 , 3],[4 , 5],[6 , 7]])
C= A.dot(B)
print(C)
# câu11
l = [(1,2,3), (2,5,3), (2,4,6,8),(3,6,4)]
t1=l[0]
t2=l[1]
t3=l[2]
t4=l[3]
T1=map(lambda t1:t1/4,[1,2,3])
T2=map(lambda t2:t2/4,[2,5,3])
T3=map(lambda t3:t3/4,[2,4,6,8])
T4=map(lambda t4:t4/4,[3,6,4])
T=list(T1)+list(T2)+list(T3)+list(T4)
print(list(T))
# câu 12
sanpham =  ['thịt', 'cá', 'trứng', 'sữa']
tensp = ['tên1', 'tên2', 'tên3', 'tên4']
giasp = [80,50,40,70]
print('Các loại sp gồm : ',sanpham)
print('Tên sp gồm : ',tensp)
print('Giá sp gồm : ',giasp)
print('----------BẢNG BÁO GIÁ----------')
for z in range (0, len(sanpham)):
    if (z==0):
        print('Loại sp: ',sanpham[0])
        print('Tên sp: ',tensp[0])
        print('Giá sp : ',giasp[0])
    elif (z==1):
        print('Loại sp: ',sanpham[1])
        print('Tên sp: ',tensp[1])
        print('Giá sp : ',giasp[1])
    elif (z==2):
        print('Loại sp: ',sanpham[2])
        print('Tên sp: ',tensp[2])
        print('Giá sp : ',giasp[2])
    else :
        print('Loại sp: ',sanpham[3])
        print('Tên sp: ',tensp[3])
        print('Giá sp : ',giasp[3])

print('Tổng giá = ',sum(giasp))
sapxep = sorted(giasp,reverse = True)
print(giasp.sort)
Dic = {
       80 : "thịt " 
            "tên1 "
            "80",
       70 : "sữa "
            "tên4 "
            "70",
       50 : "cá "
            "tên2 "
            "50",
       40 : "trứng "
            "tên3 "
            "40"
       }
print('Ba sp hàng đầu gồm : ')
for k in range (0,(len(sapxep))-1):
    if (k==0):
        print(Dic[80])
    elif (k==1):
        print(Dic[70])
    else :
        print(Dic[50])


