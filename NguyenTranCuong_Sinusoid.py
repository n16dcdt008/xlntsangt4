import numpy as np # lấy thư viện numpy
import matplotlib.pyplot as plt
A = .8 
f = 5
t = np.arange(0,1,.01) #gắn vào t 1 lits
phi = np.pi/4
x = A*np.cos(2*np.pi*f*t + phi) #nhập hàm x = cos( 2 πft + φ )
plt.plot(t,x)  # vẽ hàm x theo thời gian t
plt.axis([0,1,-1,1]) #giới hạn các trục
plt.xlabel('time in seconds') # chú thích trục x
plt.ylabel('amplitude') #chú thích trục y
plt.show() #hiển thị kết quả
"*************************************************************************"
A = .65
fs = 100
samples = 100
f = 5
phi = 0
n = np.arange(samples)#gắn vào n 1 lits
T = 1.0/fs # tính chu kỳ của T
y = A*np.cos(2*np.pi*f*n*T + phi) #tạo  y=A*cos( 2 πfn / N+ Φ )
plt.plot(y)# vẽ hàm y sử dụng kiểu và màu mặc định
plt.axis([0,100,-1,1])#giới hạn các trục
plt.xlabel('sample index')# chú thích trục x
plt.ylabel('amplitude')#chú thích trục y
plt.show()#hiển thị kết quả
"*************************************************************************"
A = .8     # như bài trên
N = 100 
f = 5
phi = 0
n = np.arange(N)#gắn vào n 1 lits
y = A*np.cos(2*np.pi*f*n/N + phi)
plt.plot(y)
plt.axis([0,100,-1,1])#giới hạn các trục
plt.xlabel('sample index')# chú thích trục x
plt.ylabel('amplitude')#chú thích trục y
plt.show()
"*************************************************************************"
f = 3
t = np.arange(0,1,.01) #gắn vào t 1 lits
phi = 0
x = np.exp(1j*(2*np.pi*f*t + phi)) # tạo x=e mũ j(2πfnT+ϕ)  
xim = np.imag(x) # nhập phần ảo của x vào xim
plt.figure(1)# tạo ra figure có tên figure 1
plt.plot(t,np.real(x))# vẽ biểu đồ phần thực của x theo trục t
plt.plot(t,xim)#vẽ biêu đồ phần ảo của x theo t
plt.axis([0,1,-1.1,1.1])#giới hạn các trục
plt.xlabel('time in seconds')# chú thích trục x
plt.ylabel('amplitude')#chú thích trục y
plt.show()
"*************************************************************************"
f = 3
N = 100
fs = 100
n = np.arange(N)#gắn vào n 1 lits
T = 1.0/fs
t = N*T
phi = 0
x = np.exp(1j*(2*np.pi*f*n*T + phi))
xim = np.imag(x)# nhập phần ảo của x vào xim
plt.figure(1)
plt.plot(n*T,np.real(x))# vẽ biểu đồ phần thực của x
plt.plot(n*T,xim)#vẽ biêu đồ phần ảo của x
plt.axis([0,t,-1.1,1.1])#giới hạn các trục
plt.xlabel('t(seconds)')# chú thích trục x
plt.ylabel('amplitude')#chú thích trục y
plt.show()
"*************************************************************************"
f = 3
N = 64 
n = np.arange(64)#gắn vào n 1 lits
phi = 0
x = np.exp(1j*(2*np.pi*f*n/N + phi))
xim = np.imag(x)# nhập phần ảo của x vào xim
plt.figure(1)# tạo ra figure có tên figure 1
plt.plot(n,np.real(x))# vẽ biểu đồ phần thực của x
plt.plot(n,xim)#vẽ biêu đồ phần ảo của x
plt.axis([0,samples,-1.1,1.1])#giới hạn các trục
plt.xlabel('sample index')# chú thích trục x
plt.ylabel('amplitude')#chú thích trục y
plt.show()
"*************************************************************************"
N = 44100 # samples
f = 440
fs = 44100
phi = 0
n = np.arange(N)#gắn vào n 1 lits
x = A*np.cos(2*np.pi*f*n/N + phi)
#scipy.io.wavfile.write(filename, rate, data)[source]
from scipy.io.wavfile import write # nhập một chuỗi thành file wav
write('sine440_1sec.wav', 44100, x)# tạo một file s440_1ec.wav thời gian 1s